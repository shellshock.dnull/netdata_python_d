# -*- coding: utf-8 -*-
# Description: example netdata python.d module
# Author: shellshock.dnull@gmail.com
# SPDX-License-Identifier: GPL-3.0-or-later

from pyvesync import VeSync
from random import SystemRandom

from bases.FrameworkServices.SimpleService import SimpleService

"""
VeSyncHumid200S300S.details = {
    'humidity': 80, # percent humidity in room
    'mist_virtual_level': 0, # Level of mist output 1 - 9
    'mist_level': 0,
    'mode': 'manual', # auto, manual, sleep
    'water_lacks': False,
    'humidity_high': False,
    'water_tank_lifted': False,
    'display': False,
    'automatic_stop_reach_target': False,
    'night_light_brightness': 0
}
VeSyncHumid200S300S.config = {
    'auto_target_humidity': 80, # percent humidity in room
    'display': True, # Display on/off
    'automatic_stop': False
}
"""

priority = 90000

ORDER = [
    'humidity',
    'mist',
    'mode',
    'light',
    'params'
]

CHARTS = {

    'humidity': {
        'options': [None, 'Humidity', 'percentage', 'humidity', 'levit.humidity', 'line'],
        'lines': [
            ['humidity'],
            ['auto_target_humidity']
        ]
    },
    'mist': {
        'options': [None, 'Mist', 'level', 'mist', 'levit.mist', 'line'],
        'lines': [
            ['mist_virtual_level'],
            ['mist_level']
        ]
    },
    'mode': {
        'options': [None, 'Mode', 'set', 'mode', 'levit.mode', 'line'],
        'lines': [
            ['manual'],
            ['auto'],
            ['sleep']
        ]
    },
    'light': {
        'options': [None, 'Night Light Brightness', 'brightness', 'light', 'levit.light', 'line'],
        'lines': [
            ['night_light_brightness']
        ]
    },
    'params': {
        'options': [None, 'Other params', 'bool', 'params', 'levit.params', 'line'],
        'lines': [
            ['water_tank_lifted'],
            ['water_lacks'],
            ['humidity_high'],
            ['display'],
            ['automatic_stop_reach_target'],
            ['automatic_stop']
        ]
    }
}


class Service(SimpleService):
    def __init__(self, configuration=None, name=None):
        SimpleService.__init__(self, configuration=configuration, name=name)
        self.order = ORDER
        self.definitions = CHARTS

        self.email = self.configuration.get('email')
        self.password = self.configuration.get('password')
        self.timezone = self.configuration.get('timezone', 'Europe/Kiev')

        self.manager = VeSync(self.email, self.password, self.timezone)
        self.manager.login()
        self.manager.update()
        for device in self.manager.fans:
            self.device = device # TODO: iter over devices

        self.data = dict()

    @staticmethod
    def check():
        # return self.manager.login()
        return True

    def get_data(self):
        self.data = dict()
        # self.device.update()
        self.device.get_details()

        try:
            details = self.device.details
            config = self.device.config
            # print(details)
            # print(config)

            self.data['humidity'] = details['humidity']
            self.data['auto_target_humidity'] = config['auto_target_humidity']

            self.data['mist_virtual_level'] = details['mist_virtual_level']
            self.data['mist_level'] = details.get('mist_level', None)
            self.data['night_light_brightness'] = details.get('night_light_brightness', None)

            self.data['manual'] = 0
            self.data['auto'] = 0
            self.data['sleep'] = 0
            mode = details.get('mode', None)
            if mode == 'manual':
                self.data['manual'] = 1
            elif mode == 'auto':
                self.data['auto'] = 1
            elif mode == 'sleep':
                self.data['sleep'] = 1

            self.data['water_lacks'] = int(details['water_lacks'])
            self.data['humidity_high'] = int(details['humidity_high'])
            self.data['water_tank_lifted'] = int(details['water_tank_lifted'])
            self.data['display'] = int(details['display'])
            self.data['automatic_stop_reach_target'] = int(details['automatic_stop_reach_target'])
            self.data['automatic_stop'] = int(config['automatic_stop'])
        except:
            self.data = None

        return self.data
