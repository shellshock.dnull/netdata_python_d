# -*- coding: utf-8 -*-
# Description: example netdata python.d module
# Author: shellshock.dnull
# SPDX-License-Identifier: GPL-3.0-or-later

import tinytuya

from bases.FrameworkServices.SimpleService import SimpleService


priority = 90000

ORDER = [
    'temperature',
    'humidity',
    'power_status',
    'power_total',
    'power_current',
    'power_power',
    'power_voltage',
    'power_fault'
]

CHARTS = {
    'temperature': {
        'options': [None, 'Temperature', 'celsius', 'temperature', 'tuya.temperature', 'line'],
        'lines': [['temperature']]
    },
    'humidity': {
        'options': [None, 'Humidity', 'percentage', 'humidity', 'tuya.humidity', 'line'],
        'lines': [['humidity']]
    },
    'power_status': {
        'options': [None, 'Status', 'bool', 'status', 'tuya.power_status', 'line'],
        'lines': [['on']]
    },
    'power_total': {
        'options': [None, 'Electricity per day', 'kWh', 'kwh', 'tuya.power_kwh', 'line'],
        'lines': [['total']]
    },
    'power_current': {
        'options': [None, 'Current', 'mA', 'current', 'tuya.power_current', 'line'],
        'lines': [['current']]
    },
    'power_power': {
        'options': [None, 'Power', 'W', 'power', 'tuya.power_w', 'line'],
        'lines': [['power']]
    },
    'power_voltage': {
        'options': [None, 'Voltage', 'V', 'voltage', 'tuya.power_voltage', 'line'],
        'lines': [['voltage']]
    },
    'power_fault': {
        'options': [None, 'Fault', 'fault', 'fault', 'tuya.power_fault', 'line'],
        'lines': [['fault']]
    }
}


class Service(SimpleService):
    def __init__(self, configuration=None, name=None):
        SimpleService.__init__(self, configuration=configuration, name=name)
        self.order = ORDER
        self.definitions = CHARTS

        self.type = self.configuration.get('type')
        self.device_id = self.configuration.get('device_id')
        self.device_key = self.configuration.get('device_key')
        self.device_host = self.configuration.get('device_host')

        self.tuya = tinytuya.OutletDevice(
            self.device_id,
            self.device_host,
            self.device_key)
        self.tuya.set_version(3.3)

        self.data = dict()
        # print('set_status() result %r' % data)

    @staticmethod
    def check():
        return True

    def get_data(self):
        self.data = dict()
        status = self.tuya.status()['dps'] or None

        if status:
            # {'1': 211, '2': 41}
            if self.type == "humid":
                self.data['temperature'] = status['1']
                self.data['humidity'] = status['2']
            elif self.type == "power":
                """
                https://github.com/jasonacox/tinytuya#version-33---plug-switch-power-strip-type
                {'1': True, Switch      bool    True/False      
                 '9': 0, Countdown 1    integer 0-86400 s
                 '17': 93, Add Electricity      integer 0-50000 kwh
                 '18': 1053, Current    integer 0-30000 mA
                 '19': 1837, Power      integer 0-50000 W
                 '20': 2244, Voltage    integer 0-5000  V
                 '21': 1,
                 '22': 601,
                 '23': 27000,
                 '24': 15112,
                 '25': 2750,
                 '26': 0, Fault fault   ov_cr
                 '38': 'memory',
                 '39': False,
                 '42': '',
                 '43': ''}
                """
                self.data["on"] = status['1']
                self.data["total"] = status['17']
                self.data["current"] = status['18']
                self.data["power"] = status['19']
                self.data["voltage"] = status['20']

        return self.data
